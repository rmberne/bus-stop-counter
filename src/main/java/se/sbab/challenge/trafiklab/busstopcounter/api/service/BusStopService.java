package se.sbab.challenge.trafiklab.busstopcounter.api.service;

import lombok.AllArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Line;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Stop;
import se.sbab.challenge.trafiklab.busstopcounter.core.service.TrafiklabDataService;

@Service
@AllArgsConstructor
public class BusStopService {

  private final TrafiklabDataService trafiklabDataService;

  @NonNull
  public Flux<Line> getTopLinesWithMoreStops(int top) {
    return trafiklabDataService.getLineStopCount(top)
        .map(lineStopCount -> Line.builder()
            .number(lineStopCount.getLineNumber())
            .numberOfStops(lineStopCount.getNumberOfStops())
            .build()
        );
  }

  @NonNull
  public Flux<Stop> getLineStops(@NonNull String lineNumber) {
    return trafiklabDataService.getLineStopCount(lineNumber)
        .flatMapMany(line -> trafiklabDataService.getLineStops(line)
            .map(stopPoint -> Stop.builder()
                .number(stopPoint.getStopPointNumber())
                .name(stopPoint.getStopPointName())
                .build()
            )
        );
  }
}
