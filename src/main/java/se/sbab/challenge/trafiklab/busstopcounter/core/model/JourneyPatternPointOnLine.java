package se.sbab.challenge.trafiklab.busstopcounter.core.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Value;

@Value
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class JourneyPatternPointOnLine implements Result {
  String lineNumber;
  String directionCode;
  String journeyPatternPointNumber;
  String lastModifiedUtcDateTime;
  String existsFromDate;
}
