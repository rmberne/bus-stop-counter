package se.sbab.challenge.trafiklab.busstopcounter.api.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Stop {
  String number;
  String name;
}
