package se.sbab.challenge.trafiklab.busstopcounter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "trafiklab")
public class TrafiklabProperties {
  private String apiUrl;
  private String apiKey;
}
