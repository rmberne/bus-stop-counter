package se.sbab.challenge.trafiklab.busstopcounter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfiguration {

  private static final int MAX_IN_MEMORY_SIZE = 10 * 1024 * 1024;

  @Bean
  WebClient webClient(WebClient.Builder webClientBuilder) {
    return webClientBuilder
        .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(MAX_IN_MEMORY_SIZE))
        .build();
  }
}
