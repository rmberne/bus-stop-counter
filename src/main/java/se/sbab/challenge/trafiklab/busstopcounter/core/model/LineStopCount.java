package se.sbab.challenge.trafiklab.busstopcounter.core.model;

import lombok.Value;

import java.util.Set;

@Value(staticConstructor = "of")
public class LineStopCount {
  String lineNumber;
  int numberOfStops;

  Set<String> stopNumbers;
}
