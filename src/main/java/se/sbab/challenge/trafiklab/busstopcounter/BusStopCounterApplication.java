package se.sbab.challenge.trafiklab.busstopcounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import se.sbab.challenge.trafiklab.busstopcounter.config.TrafiklabProperties;

@SpringBootApplication
@EnableCaching
@EnableConfigurationProperties(TrafiklabProperties.class)
public class BusStopCounterApplication {

  public static void main(String[] args) {
    SpringApplication.run(BusStopCounterApplication.class, args);
  }

}
