package se.sbab.challenge.trafiklab.busstopcounter.core.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Value;

@Value
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class TrafiklabResponse<T extends Result> {
  int statusCode;
  int executionTime;
  String message;
  ResponseData<T> responseData;
}
