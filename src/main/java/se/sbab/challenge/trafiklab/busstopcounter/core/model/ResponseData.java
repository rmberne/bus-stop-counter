package se.sbab.challenge.trafiklab.busstopcounter.core.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Value;

import java.util.List;

@Value
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class ResponseData<T extends Result> {
  String version;
  String type;
  List<T> result;
}
