package se.sbab.challenge.trafiklab.busstopcounter.core.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Value;

@Value
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class StopPoint implements Result {
  String stopPointNumber;
  String stopPointName;
  String stopAreaNumber;
  String locationNorthingCoordinate;
  String locationEastingCoordinate;
  String zoneShortName;
  String stopAreaTypeCode;
  String lastModifiedUtcDateTime;
  String existsFromDate;
}
