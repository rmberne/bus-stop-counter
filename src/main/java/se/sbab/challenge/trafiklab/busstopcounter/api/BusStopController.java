package se.sbab.challenge.trafiklab.busstopcounter.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Line;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Stop;
import se.sbab.challenge.trafiklab.busstopcounter.api.service.BusStopService;

@RestController
@RequestMapping("api/v1/bus-stops-counter/lines")
@AllArgsConstructor
public class BusStopController {

  private final BusStopService busStopService;

  @GetMapping
  Flux<Line> getLines(@RequestParam(defaultValue = "10") int top) {
    return busStopService.getTopLinesWithMoreStops(top);
  }

  @GetMapping("{lineNumber}/stops")
  Flux<Stop> getStops(@PathVariable String lineNumber) {
    return busStopService.getLineStops(lineNumber);
  }

}
