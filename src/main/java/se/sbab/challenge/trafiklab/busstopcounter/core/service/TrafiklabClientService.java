package se.sbab.challenge.trafiklab.busstopcounter.core.service;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.sbab.challenge.trafiklab.busstopcounter.config.TrafiklabProperties;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.JourneyPatternPointOnLine;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.StopPoint;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.TrafiklabResponse;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;

@Service
@AllArgsConstructor
public class TrafiklabClientService {

  private static final String PARAM_KEY = "key";
  private static final String PARAM_MODEL = "model";
  private static final String PARAM_DEFAULT_TRANSPORT_MODE_CODE = "DefaultTransportModeCode";

  private static final String JOURNEY_MODE = "jour";
  private static final String STOP_MODE = "stop";
  private static final String BUS_TRANSPORT_MODE = "BUS";

  private final WebClient webClient;
  private final TrafiklabProperties properties;

  @Cacheable(cacheNames = "journeys")
  @NonNull
  public Mono<Map<String, Collection<JourneyPatternPointOnLine>>> retrieveBusJourneys() {
    final var uri = UriComponentsBuilder.fromHttpUrl(properties.getApiUrl())
        .queryParam(PARAM_KEY, properties.getApiKey())
        .queryParam(PARAM_MODEL, JOURNEY_MODE)
        .queryParam(PARAM_DEFAULT_TRANSPORT_MODE_CODE, BUS_TRANSPORT_MODE)
        .toUriString();
    return webClient.get().uri(uri).acceptCharset(StandardCharsets.UTF_8).retrieve()
        .bodyToMono(new ParameterizedTypeReference<TrafiklabResponse<JourneyPatternPointOnLine>>() { })
        .flatMapIterable(response -> response.getResponseData().getResult())
        .collectMultimap(JourneyPatternPointOnLine::getLineNumber)
        .cache();
  }

  @Cacheable(cacheNames = "stops")
  @NonNull
  public Flux<StopPoint> retrieveBusStops() {
    final var uri = UriComponentsBuilder.fromHttpUrl(properties.getApiUrl())
        .queryParam(PARAM_KEY, properties.getApiKey())
        .queryParam(PARAM_MODEL, STOP_MODE)
        .toUriString();
    return webClient.get().uri(uri).acceptCharset(StandardCharsets.UTF_8).retrieve()
        .bodyToMono(new ParameterizedTypeReference<TrafiklabResponse<StopPoint>>() { })
        .flatMapIterable(response -> response.getResponseData().getResult())
        .cache();
  }
}
