package se.sbab.challenge.trafiklab.busstopcounter.core.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.JourneyPatternPointOnLine;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.LineStopCount;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.StopPoint;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class TrafiklabDataService {

  private static final Duration INIT_TIMEOUT_IN_SECONDS = Duration.ofSeconds(15);

  private final TrafiklabClientService trafiklabClientService;

  @PostConstruct
  public void init() {
    trafiklabClientService.retrieveBusJourneys().block(INIT_TIMEOUT_IN_SECONDS);
    trafiklabClientService.retrieveBusStops().collectList().block(INIT_TIMEOUT_IN_SECONDS);
  }

  @NonNull
  public Flux<LineStopCount> getLineStopCount(int top) {
    return trafiklabClientService.retrieveBusJourneys()
        .flatMapIterable(Map::entrySet)
        .sort(Comparator.comparingInt(entry -> entry.getValue().size()))
        .takeLast(top)
        .map(this::mapLineStopCount)
        .sort(Comparator.comparingInt(LineStopCount::getNumberOfStops).reversed());
  }

  @NonNull
  public Mono<LineStopCount> getLineStopCount(@NonNull String lineNumber) {
    return trafiklabClientService.retrieveBusJourneys()
        .flatMapIterable(Map::entrySet)
        .filter(entry -> entry.getKey().equals(lineNumber))
        .map(this::mapLineStopCount)
        .next();
  }

  @NonNull
  public Flux<StopPoint> getLineStops(@NonNull LineStopCount line) {
    return trafiklabClientService.retrieveBusStops()
        .filter(stopPoint -> line.getStopNumbers().contains(stopPoint.getStopPointNumber()));
  }

  private Set<String> getStopNumbers(Collection<JourneyPatternPointOnLine> journeys) {
    return journeys.stream().map(JourneyPatternPointOnLine::getJourneyPatternPointNumber)
        .collect(Collectors.toSet());
  }

  private LineStopCount mapLineStopCount(Map.Entry<String, Collection<JourneyPatternPointOnLine>> entry) {
    // remove repetitions of stops that are in both directions
    final var stopsNumbers = getStopNumbers(entry.getValue());
    return LineStopCount.of(entry.getKey(), stopsNumbers.size(), stopsNumbers);
  }

}
