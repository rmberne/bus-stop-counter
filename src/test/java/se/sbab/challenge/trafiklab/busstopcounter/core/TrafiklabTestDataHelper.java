package se.sbab.challenge.trafiklab.busstopcounter.core;

import se.sbab.challenge.trafiklab.busstopcounter.core.model.JourneyPatternPointOnLine;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.LineStopCount;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.StopPoint;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TrafiklabTestDataHelper {

  private TrafiklabTestDataHelper() {
  }

  public static Map<String, Collection<JourneyPatternPointOnLine>> createJourneyData() {
    final var journeyData = new LinkedHashMap<String, Collection<JourneyPatternPointOnLine>>();
    for (var lineNumber = 1; lineNumber <= 15; lineNumber++) {
      journeyData.putAll(createJourneyPointData(String.valueOf(lineNumber), lineNumber * 10));
    }
    return Collections.unmodifiableMap(journeyData);
  }

  public static List<StopPoint> createStopPointData() {
    return IntStream.rangeClosed(1, 1000)
        .mapToObj(stopNumber -> new StopPoint(String.valueOf(stopNumber), "Point " + stopNumber, "", "", "", "", "", "", ""))
        .collect(Collectors.toList());
  }

  public static List<LineStopCount> createLineStopCountData() {
    return IntStream.rangeClosed(1, 10)
        .mapToObj(lineNumber -> {
          final var stopNumbers = IntStream.rangeClosed(1, (lineNumber * 10))
              .mapToObj(String::valueOf)
              .collect(Collectors.toSet());
          return LineStopCount.of(String.valueOf(lineNumber), stopNumbers.size(), stopNumbers);
        })
        .sorted(Comparator.comparingInt(LineStopCount::getNumberOfStops).reversed())
        .collect(Collectors.toList());
  }

  public static LineStopCount createLineStopCountData(int lineNumber) {
    final var stopNumbers = IntStream.rangeClosed(1, (lineNumber * 10))
        .mapToObj(String::valueOf)
        .collect(Collectors.toSet());
    return LineStopCount.of(String.valueOf(lineNumber), stopNumbers.size(), stopNumbers);
  }

  private static Map<String, List<JourneyPatternPointOnLine>> createJourneyPointData(String lineNumber, int numberOfStops) {
    return Stream.concat(
        createJourneyPointData(lineNumber, "1", numberOfStops),
        createJourneyPointData(lineNumber, "2", numberOfStops)
    ).collect(Collectors.groupingBy(JourneyPatternPointOnLine::getLineNumber));
  }

  private static Stream<JourneyPatternPointOnLine> createJourneyPointData(String lineNumber, String direction, int numberOfStops) {
    return IntStream.rangeClosed(1, numberOfStops)
        .mapToObj(stopNumber -> new JourneyPatternPointOnLine(lineNumber, direction, String.valueOf(stopNumber), null, null));
  }

}
