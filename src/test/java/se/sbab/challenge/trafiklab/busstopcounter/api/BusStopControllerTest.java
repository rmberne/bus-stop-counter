package se.sbab.challenge.trafiklab.busstopcounter.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Line;
import se.sbab.challenge.trafiklab.busstopcounter.api.service.BusStopService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.mockito.BDDMockito.given;

@WebFluxTest(BusStopController.class)
class BusStopControllerTest {

  @Autowired
  private WebTestClient client;

  @MockBean
  private BusStopService busStopService;

  @Test
  void shouldGetTop10LinesWithMoreStops() {
    given(busStopService.getTopLinesWithMoreStops(10)).willReturn(Flux.fromIterable(createLineData()));
    final var uri = UriComponentsBuilder.fromPath("/api/v1/bus-stops-counter/lines")
        .queryParam("top", 10)
        .toUriString();
    client.get().uri(uri).exchange()
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$.[0].number").isEqualTo("10")
        .jsonPath("$.[0].numberOfStops").isEqualTo(10)
        .jsonPath("$.[1].number").isEqualTo("9")
        .jsonPath("$.[1].numberOfStops").isEqualTo(9)
        .jsonPath("$.[2].number").isEqualTo("8")
        .jsonPath("$.[2].numberOfStops").isEqualTo(8)
        .jsonPath("$.[3].number").isEqualTo("7")
        .jsonPath("$.[3].numberOfStops").isEqualTo(7)
        .jsonPath("$.[4].number").isEqualTo("6")
        .jsonPath("$.[4].numberOfStops").isEqualTo(6)
        .jsonPath("$.[5].number").isEqualTo("5")
        .jsonPath("$.[5].numberOfStops").isEqualTo(5)
        .jsonPath("$.[6].number").isEqualTo("4")
        .jsonPath("$.[6].numberOfStops").isEqualTo(4)
        .jsonPath("$.[7].number").isEqualTo("3")
        .jsonPath("$.[7].numberOfStops").isEqualTo(3)
        .jsonPath("$.[8].number").isEqualTo("2")
        .jsonPath("$.[8].numberOfStops").isEqualTo(2)
        .jsonPath("$.[9].number").isEqualTo("1")
        .jsonPath("$.[9].numberOfStops").isEqualTo(1);
  }

  private List<Line> createLineData() {
    return IntStream.rangeClosed(1, 10)
        .mapToObj(number -> Line.builder().number(String.valueOf(number)).numberOfStops(number).build())
        .sorted(Comparator.comparingInt(Line::getNumberOfStops).reversed())
        .collect(Collectors.toList());
  }
}