package se.sbab.challenge.trafiklab.busstopcounter.api.service;

import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Line;
import se.sbab.challenge.trafiklab.busstopcounter.api.model.Stop;
import se.sbab.challenge.trafiklab.busstopcounter.core.service.TrafiklabDataService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static se.sbab.challenge.trafiklab.busstopcounter.core.TrafiklabTestDataHelper.createLineStopCountData;
import static se.sbab.challenge.trafiklab.busstopcounter.core.TrafiklabTestDataHelper.createStopPointData;

@ExtendWith(MockitoExtension.class)
class BusStopServiceTest {

  @Mock
  private TrafiklabDataService trafiklabDataServiceMock;

  @Test
  void getTopLinesWithMoreStopsShouldReturnLines() {
    given(trafiklabDataServiceMock.getLineStopCount(10)).willReturn(Flux.fromIterable(createLineStopCountData()));

    final var subject = new BusStopService(trafiklabDataServiceMock);
    final var result = subject.getTopLinesWithMoreStops(10).collectList().block();

    assertThat(result).hasSize(10)
        .containsExactly(
            Line.builder().number("10").numberOfStops(100).build(),
            Line.builder().number("9").numberOfStops(90).build(),
            Line.builder().number("8").numberOfStops(80).build(),
            Line.builder().number("7").numberOfStops(70).build(),
            Line.builder().number("6").numberOfStops(60).build(),
            Line.builder().number("5").numberOfStops(50).build(),
            Line.builder().number("4").numberOfStops(40).build(),
            Line.builder().number("3").numberOfStops(30).build(),
            Line.builder().number("2").numberOfStops(20).build(),
            Line.builder().number("1").numberOfStops(10).build()
        );
  }

  @Test
  void getLineStopsShouldReturnStopsForAGivenLine() {
    final var lineStopCount = createLineStopCountData(10);
    final var stopPoints = createStopPointData().subList(0, 10);
    given(trafiklabDataServiceMock.getLineStopCount("10")).willReturn(Mono.just(lineStopCount));
    given(trafiklabDataServiceMock.getLineStops(lineStopCount)).willReturn(Flux.fromIterable(stopPoints));

    final var subject = new BusStopService(trafiklabDataServiceMock);
    final var result = subject.getLineStops("10").collectList().block();

    assertThat(result).hasSize(10)
        .extracting(Stop::getNumber, Stop::getName)
        .contains(
            Tuple.tuple("1", "Point 1"),
            Tuple.tuple("2", "Point 2"),
            Tuple.tuple("3", "Point 3"),
            Tuple.tuple("4", "Point 4"),
            Tuple.tuple("5", "Point 5"),
            Tuple.tuple("6", "Point 6"),
            Tuple.tuple("7", "Point 7"),
            Tuple.tuple("8", "Point 8"),
            Tuple.tuple("9", "Point 9"),
            Tuple.tuple("10", "Point 10")
        );
  }

}