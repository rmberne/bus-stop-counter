package se.sbab.challenge.trafiklab.busstopcounter.core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import se.sbab.challenge.trafiklab.busstopcounter.config.TrafiklabProperties;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.JourneyPatternPointOnLine;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.ResponseData;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.TrafiklabResponse;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class TrafiklabClientServiceTest {

  @Mock
  private WebClient webClientMock;

  @Mock
  @SuppressWarnings("rawtypes")
  private WebClient.RequestHeadersUriSpec requestHeadersUriSpecMock;

  @Mock
  private WebClient.ResponseSpec responseSpecMock;

  private final TrafiklabProperties properties = new TrafiklabProperties();

  @SuppressWarnings("unchecked")
  @BeforeEach
  public void beforeEach() {
    webClientMock = mock(WebClient.class);
    given(webClientMock.get()).willReturn(requestHeadersUriSpecMock);
    given(requestHeadersUriSpecMock.uri(anyString())).willReturn(requestHeadersUriSpecMock);
    given(requestHeadersUriSpecMock.acceptCharset(StandardCharsets.UTF_8)).willReturn(requestHeadersUriSpecMock);
    given(requestHeadersUriSpecMock.retrieve()).willReturn(responseSpecMock);
    given(responseSpecMock.bodyToMono(any(ParameterizedTypeReference.class))).willReturn(Mono.just(getTrafiklabResponse()));

    properties.setApiUrl("https://api.local.se");
  }

  @Test
  void retrieveBussJourneysShouldReturnData() {
    final var trafiklabClientService = new TrafiklabClientService(webClientMock, properties);
    StepVerifier.create(trafiklabClientService.retrieveBusJourneys())
        .expectNext(Map.of("1", getTrafiklabResponse().getResponseData().getResult()));
  }

  private TrafiklabResponse<JourneyPatternPointOnLine> getTrafiklabResponse() {
    final var result = List.of(
        new JourneyPatternPointOnLine("1", "1", "123", "2020-03-25 00:02", "2020-03-25 00:02"),
        new JourneyPatternPointOnLine("1", "2", "123", "2020-03-25 00:02", "2020-03-25 00:02")
    );
    final var responseData = new ResponseData<>("2020-03-25 00:02", "JourneyPatternPointOnLine", result);
    return new TrafiklabResponse<>(200, 0, null, responseData);
  }
}