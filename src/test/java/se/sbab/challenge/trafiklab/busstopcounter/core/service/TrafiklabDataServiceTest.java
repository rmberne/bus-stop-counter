package se.sbab.challenge.trafiklab.busstopcounter.core.service;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.LineStopCount;
import se.sbab.challenge.trafiklab.busstopcounter.core.model.StopPoint;

import java.time.Duration;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static se.sbab.challenge.trafiklab.busstopcounter.core.TrafiklabTestDataHelper.createJourneyData;
import static se.sbab.challenge.trafiklab.busstopcounter.core.TrafiklabTestDataHelper.createStopPointData;

@ExtendWith(MockitoExtension.class)
class TrafiklabDataServiceTest {

  @Mock
  private TrafiklabClientService trafiklabClientServiceMock;

  @Test
  void initShouldBlockUntilCacheDataIsRetrieved() {
    given(trafiklabClientServiceMock.retrieveBusJourneys()).willReturn(Mono.empty());
    given(trafiklabClientServiceMock.retrieveBusStops()).willReturn(Flux.empty());

    final var subject = new TrafiklabDataService(trafiklabClientServiceMock);

    subject.init();

    verify(trafiklabClientServiceMock).retrieveBusJourneys();
    verify(trafiklabClientServiceMock).retrieveBusStops();
  }

  @Test
  void initShouldFailWhenCacheDataExceedsTheInitTimeout() {
    given(trafiklabClientServiceMock.retrieveBusJourneys())
        .willReturn(Mono.delay(Duration.ofMinutes(1)).then(Mono.empty()));

    final var subject = new TrafiklabDataService(trafiklabClientServiceMock);

    assertThatThrownBy(subject::init).isInstanceOf(IllegalStateException.class);
  }

  @Test
  void getLineStopCountShouldReturnTopFiveLineStopCount() {
    given(trafiklabClientServiceMock.retrieveBusJourneys()).willReturn(Mono.just(createJourneyData()));

    final var subject = new TrafiklabDataService(trafiklabClientServiceMock);
    final var result = subject.getLineStopCount(5).collectList().block();

    assertThat(result)
        .hasSize(5)
        .extracting(LineStopCount::getLineNumber, LineStopCount::getNumberOfStops)
        .containsExactly(
            Tuple.tuple("15", 150),
            Tuple.tuple("14", 140),
            Tuple.tuple("13", 130),
            Tuple.tuple("12", 120),
            Tuple.tuple("11", 110)
        );
  }

  @Test
  void getLineStopCountShouldReturnOneLineStopCountForAGivenLineNumber() {
    final var journeyData = createJourneyData();
    given(trafiklabClientServiceMock.retrieveBusJourneys()).willReturn(Mono.just(journeyData));

    final var subject = new TrafiklabDataService(trafiklabClientServiceMock);
    final var result = subject.getLineStopCount("1").block();

    assertThat(result).extracting(LineStopCount::getLineNumber)
        .isEqualTo("1");
    assertThat(result).extracting(LineStopCount::getNumberOfStops)
        .isEqualTo(10);
    assertThat(result).extracting(LineStopCount::getStopNumbers)
        .asInstanceOf(InstanceOfAssertFactories.iterable(String.class))
        .contains("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
  }

  @Test
  void getLineStopCountShouldReturnStopPointsForAGivenLineNumber() {
    given(trafiklabClientServiceMock.retrieveBusStops()).willReturn(Flux.fromIterable(createStopPointData()));
    final var lineStopCount = LineStopCount.of("1", 10, Set.of("11", "110", "12", "13", "14", "15", "16", "17", "18", "19"));

    final var subject = new TrafiklabDataService(trafiklabClientServiceMock);
    final var result = subject.getLineStops(lineStopCount).collectList().block();

    assertThat(result).hasSize(10)
        .extracting(StopPoint::getStopPointNumber)
        .containsExactlyInAnyOrder("11", "110", "12", "13", "14", "15", "16", "17", "18", "19");
  }
}