# Bus Stop Counter #

This is an application to find out which bus lines that have the most bus stops
on their route.

It provides two endpoints:
```
api/v1/bus-stops-counter/lines?top={desiredNumber}
api/v1/bus-stops-counter/lines/{lineNumber}/stops
```

### How do I get set up? ###

* Install JDK 11
* Run the tests
    ```shell script
    ./gradlew test
    ```
* Run the application
    ```shell script
    ./gradlew bootRun
    ```

The commands above are considering a Linux/MacOS setup. Please use `./gradlew.bat` for Windows.